from simpleai.search import SearchProblem, breadth_first, depth_first, greedy, astar
import random


#El primer elemento de la lista corresponde al robot. En un elemento, el primer valor es la fila, el segundo valor
#es la columna, y el tercero es la temperatura
INITIAL = (
        (3,3,0),
        )

#En la verificacion de si el estado actual es goal, hay que sacar asignar None al tercer valor a cada elemento
#(transformar la tupla a lista)
GOAL = (
    (3,3,None),
    )



def resolver(metodo_busqueda, posiciones_aparatos):
    global INITIAL
    global GOAL
    
    INITIAL = tuple2list(INITIAL)
    posiciones_aparatos = tuple2list(posiciones_aparatos)
    for x in posiciones_aparatos:
        x.append(300)        
        INITIAL.append(x)    
    INITIAL = list2tuple(INITIAL)
    posiciones_aparatos = list2tuple(posiciones_aparatos)

    
    GOAL = tuple2list(GOAL)
    for x in posiciones_aparatos:
        GOAL.append((3,3,None))
    GOAL = list2tuple(GOAL)
    
    
    if (metodo_busqueda == 'breadth_first'):
        resultado = breadth_first(Problem(INITIAL), graph_search=True)
    if (metodo_busqueda == 'depth_first'):
        resultado = depth_first(Problem(INITIAL), graph_search=True)
    if (metodo_busqueda == 'greedy'):
        resultado = greedy(Problem(INITIAL), graph_search=True)
    if (metodo_busqueda == 'astar'):
        resultado = astar(Problem(INITIAL), graph_search=True)

    return resultado
    
    

#Funcion Case/switch para el metodo de busqueda
def case_metodo_busqueda(argument):
    switcher = {
        'breadth_first': breadth_first(Problem(INITIAL), graph_search=True),
        'depth_first': depth_first(Problem(INITIAL), graph_search=True),
        'greedy': greedy(Problem(INITIAL), graph_search=True),
        'astar': astar(Problem(INITIAL), graph_search=True),
    }
    return switcher.get(argument, None)


def tuple2list(t):
    return [list(row) for row in t]


def list2tuple(t):
    return tuple(tuple(row) for row in t)


def find_number(number, state):
    for row_index, row in enumerate(state):
        for column_index, piece in enumerate(row):
            if piece == number:
                return row_index, column_index


def find_aparatos_in_robot(robot_row, robot_column, state):
    state = tuple2list(state)
    for x in state:
        if (x[0] == robot_row):
            if (x[1] == robot_column):
                state = list2tuple(state)
                return True
    state = list2tuple(state)
    return False


class Problem(SearchProblem):
    def is_goal(self, state):
        state_aux = tuple2list(state)
        for x in state_aux:
            #Elimino la temperatura en cada elemento del state_aux
            #x.index(len(x) - 1) hace referencia a la última posicion de cada elemento, ya que arranca en 0
            #x.remove(x.index(len(x) - 1))
            x[len(x) - 1] = None            
        
        return state_aux == GOAL


    def cost(self, state1, action, state2):
        return 1


    def actions(self, state):
        #zero_row, zero_column = find_number(0, state)
        robot_row = state[0][0]
        robot_column = state[0][1]

        available_actions = []

        #state = tuple2list(state)
        #state[0][2] = 1
        #print (state[0])
        #state[0][2] = 300
        #print (state[0])
        
        aparato_quemado = False
        for item in state:
            if (item != state[0]): #El robot no se quema, por lo tanto no se evalua
                if (item[2] > 500):
                    aparato_quemado = True

        if (not aparato_quemado):
            enfriar = find_aparatos_in_robot(robot_row, robot_column, state)
            state = tuple2list(state)
            
            if (enfriar):                
                #available_actions.append(state[robot_row][robot_column][150])
                state[0][2] = 150
                #available_actions.append(state[0][2])
                available_actions.append(state[0])                
                
            if robot_row > 0: #El robot no esta en la primer fila => Subo el robot una fila                                
                #state[0] es el elemento robot (fila,columna,temperatura)
                #state[0][0] es la fila del robot
                state[0][0] = state[0][0] - 1
                state[0][2] = 300 #Para saber si el robot se desplazo, o enfrio
                #available_actions.append(state[robot_row - 1][robot_column][0])
                available_actions.append(state[0])
                    
            if robot_row < 3: #El robot no esta en la ultima fila => bajo el robot una fila
                state[0][0] = state[0][0] + 1
                state[0][2] = 300 #Para saber si el robot se desplazo, o enfrio
                available_actions.append(state[0])
                    
            if robot_column > 0: #El robot no esta en la primer columna => muevo el robot a la izquierda
                state[0][1] = state[0][1] - 1
                state[0][2] = 300 #Para saber si el robot se desplazo, o enfrio
                available_actions.append(state[0])
                    
            if robot_column < 3: #El robot no esta en la ultima columna => muevo el robot a la derecha
                state[0][1] = state[0][1] + 1
                state[0][2] = 300 #Para saber si el robot se desplazo, o enfrio
                available_actions.append(state[0])

            state = list2tuple(state)

        return available_actions
    

    def result(self, state, action):
        state = tuple2list(state)
        if (state[0][2] == 150): #La accion fue enfriar los aparatos
            #Enfriar todos los aparatos que se encuentran en la posición del robot
            for x in state:
                if (x != state[0]): #El elemento x no es el robot
                    if (x[0] == state[0][0]): #La fila del elemento == fila del robot
                        if (x[1] == state[0][1]): #La columna del elemento == columna del robot                            
                            x[2] = x[2] - 150 #Enfriar                            
        if (state[0][2] == 300): #La accion fue mover el robot, hay que mover uno de los aparatos(en caso de que hubiese
                                #un aparato en la posicion del robot (que esta registrada en la posicion del action))

            #Verifico si habia por lo menos un aparato en la posicion anterior del robot
            habia_aparato = False
            for x in state:
                if (x != state[0]): #x no es el robot
                    if (x[0] == action[0]): #x tiene la misma fila que action
                        if (x[1] == action[1]): #x tiene la misma columna que action
                            habia_aparato = True
            if (habia_aparato):
                action[0] = state[0][0]
                action[1] = state[0][1]                
        #Incrementar temperatura a todos los aparatos excepto al robot, a action y a los que estan en (3,3)
        for x in state:
            if (x != state[0]): #x no es el robot                
                if ((x[0] != 3) and (x[1] != 3)): #x no esta en la salida
                                                  #(los aparatos en la salida no levantan temperatura)
                    x[2] = x[2] + 25
        

        state = list2tuple(state)
        
        return state


    def heuristic(self,state):
        pass
    
